<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220103153403 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD38B53C32');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE88CCE5');
        $this->addSql('DROP INDEX IDX_D34A04AD38B53C32 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADE88CCE5 ON product');
        $this->addSql('ALTER TABLE product ADD color_id INT NOT NULL, ADD company_id INT NOT NULL, DROP color_id_id, DROP company_id_id');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD7ADA1FB5 FOREIGN KEY (color_id) REFERENCES color (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD7ADA1FB5 ON product (color_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD979B1AD6 ON product (company_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD7ADA1FB5');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD979B1AD6');
        $this->addSql('DROP INDEX IDX_D34A04AD7ADA1FB5 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD979B1AD6 ON product');
        $this->addSql('ALTER TABLE product ADD color_id_id INT NOT NULL, ADD company_id_id INT NOT NULL, DROP color_id, DROP company_id');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD38B53C32 FOREIGN KEY (company_id_id) REFERENCES company (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE88CCE5 FOREIGN KEY (color_id_id) REFERENCES color (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_D34A04AD38B53C32 ON product (company_id_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADE88CCE5 ON product (color_id_id)');
    }
}
