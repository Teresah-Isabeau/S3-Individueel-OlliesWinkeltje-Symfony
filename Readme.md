# Ollies winkeltje backend

## Requirements

- download docker

## Installation

```bash
cp .env.dist .env
 #fill env variables...
 
docker-compose build
docker-compose up


docker-compose exec php bash
composer install
php bin/console doctrine:migrations:migrate
```

## Docker

To enter a  container to run certain commands use:

```shell
docker-compose exec php sh
```

then u can use commands like `composer req <package>`
