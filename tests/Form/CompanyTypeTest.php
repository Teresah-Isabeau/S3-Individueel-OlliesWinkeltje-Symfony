<?php

namespace App\Tests\Form;

use App\Entity\Company;
use App\Form\CompanyType;
use Symfony\Component\Form\Test\TypeTestCase;

class CompanyTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'name' => 'test',
        ];
        $model = new Company();
        // $model will retrieve data from the form submission; pass it as the second argument
        $form = $this->factory->create(CompanyType::class, $model);

        $expected = new Company();
        $expected->setName('test');

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        // check that $model was modified as expected when the form was submitted
        $this->assertEquals($expected, $model);
    }
}
