<?php

namespace App\Tests\Form;

use App\Entity\Color;
use App\Form\ColorType;
use Symfony\Component\Form\Test\TypeTestCase;

class ColorTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'name' => 'test',
        ];
        $model = new Color();
        // $model will retrieve data from the form submission; pass it as the second argument
        $form = $this->factory->create(ColorType::class, $model);

        $expected = new Color();
        $expected->setName('test');

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        // check that $model was modified as expected when the form was submitted
        $this->assertEquals($expected, $model);
    }
}
