<?php

namespace App\Form;

use App\Entity\Color;
use App\Entity\Company;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name' ,TextType::class)
            ->add('price', IntegerType::class)
            ->add('productCode', IntegerType::class)
            ->add('stock', IntegerType::class)
            ->add('description', TextType::class)
            ->add('appearanceYear', DateType::class,[
                'widget' => 'single_text',
            ])
            ->add('color', EntityType::class, [
                'class' => Color::class,
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
