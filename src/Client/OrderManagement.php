<?php

namespace App\Client;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class OrderManagement
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function post($data)
    {
        $response = $this->client->request(
          'POST',
          'http://csharp/api/products',
            ['json' => $data]
        );
        $statusCode = $response->getStatusCode();
        if($statusCode != 200){
            return false;
        }
        return true;
    }
}