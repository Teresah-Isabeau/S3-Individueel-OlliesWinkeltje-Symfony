<?php

namespace App\Service;

use App\Client\OrderManagement;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductService extends AbstractService
{
    private ProductRepository $repository;
    private OrderManagement $orderManagement;

    public function __construct(ProductRepository $repository, EntityManagerInterface $entityManager, OrderManagement $orderManagement)
    {
        parent::__construct($entityManager);
        $this->repository = $repository;
        $this->orderManagement = $orderManagement;
    }

    public function getRepository(): ServiceEntityRepository
    {
        return $this->repository;
    }

    public function create($entity): ?Product
    {
        $product = parent::create($entity);
        $this->orderManagement->post([
            'name' => $entity->getName(),
            'productCode' => $entity->getProductCode(),
        ]);
        return $product;
    }
}
