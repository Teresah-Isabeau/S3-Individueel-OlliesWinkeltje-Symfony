<?php

namespace App\Service;

use App\Repository\ColorRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class ColorService extends AbstractService
{
    private ColorRepository $colorRepository;

    public function __construct(EntityManagerInterface $entityManager, ColorRepository $colorRepository)
    {
        parent::__construct($entityManager);
        $this->colorRepository = $colorRepository;
    }

    public function getRepository(): ServiceEntityRepository
    {
        return $this->colorRepository;
    }
}
