<?php

namespace App\Service;

use App\Repository\CompanyRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class CompanyService extends AbstractService
{
    private CompanyRepository $companyRepository;

    public function __construct(EntityManagerInterface $entityManager, CompanyRepository $companyRepository)
    {
        parent::__construct($entityManager);
        $this->companyRepository = $companyRepository;
    }

    public function getRepository(): ServiceEntityRepository
    {
        return $this->companyRepository;
    }
}
