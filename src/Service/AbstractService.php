<?php

namespace App\Service;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    abstract public function getRepository(): ServiceEntityRepository;

    public function findAll(): array
    {
        return $this->getRepository()->findAll();
    }

    public function find(int $id)
    {
        return $this->getRepository()->find($id);
    }

    public function create($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }

    public function update($entity)
    {
        $this->entityManager->flush();
        return $entity;
    }

    public function delete($entity): bool
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
        return true;
    }
}
