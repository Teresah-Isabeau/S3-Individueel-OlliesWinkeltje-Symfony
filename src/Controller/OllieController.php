<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OllieController extends AbstractController
{
    public function number(): Response
    {
        $number = random_int(0, 1000);

//        $products = $productService->getByProperties(['color' => $color]);
//        $products = $color->getProducts();
//
//        foreach ($products as $product) {
//            $product->setColor($color);
//        }
//        $color->addProducts($products);

        return new Response(
            '<html lang="en"><body>Lucky number: '.$number.'</body></html>'
        );
    }
}
