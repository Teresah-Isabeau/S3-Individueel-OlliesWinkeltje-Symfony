<?php

namespace App\Controller;

use App\Entity\Company;
use App\Form\ColorType;
use App\Form\CompanyType;
use App\Service\CompanyService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractFOSRestController
{
    private CompanyService $companyService;

    public function __construct(
        CompanyService $companyService
    ){
        $this->companyService = $companyService;
    }

    #[Route('/companys', name: 'all_companys', methods: ['GET'])]
    public function index(): Response
    {
        $companys = $this->companyService->findAll();
        return $this->handleView($this->view($companys));
    }

    #[Route('/companys/add/', name: 'add_company', methods: ['POST'])]
    public function addCompany(Request $request): Response
    {
        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView($this->view($form));
        }

        $company = $this->companyService->create($company);
        return $this->handleView($this->view($company));
    }

    #[Route('/companys/{id}', name: 'get_one_company', methods: ['GET'])]
    public function getCompany($id): Response
    {
        $company = $this->companyService->find($id);
        return $this->handleView($this->view($company));
    }

    #[Route('/companys/{id}/update/', name: 'update_one_company', methods: ['PATCH'])]
    public function  updateCompany($id, Request $request ): Response
    {
        $company = $this->companyService->find($id);
        if(!$company){
            throw $this->createNotFoundException(
                'No company found'
            );
        }
        $form = $this->createForm(CompanyType::class, $company, ['method' => 'patch']);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView($this->view($form, 400));
        }

        $company = $this->companyService->update($company);
        return $this->handleView($this->view($company));
    }

    #[Route('companys/{id}/delete/', name: 'delete_one_company', methods: ['DELETE'])]
    public function deleteCompany($id): Response
    {
        $company = $this->companyService->find($id);
        if(!$company){
            throw $this->createNotFoundException(
                'No company found'
            );
        }
        $this->companyService->delete($company);
        return $this->handleView($this->view(['status' => 'ok']));
    }
}