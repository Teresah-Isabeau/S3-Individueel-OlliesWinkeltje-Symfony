<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Service\ProductService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractFOSRestController
{
    private ProductService $productService;

    public function __construct(
        ProductService $productService
    ) {
        $this->productService = $productService;
    }

    #[Route('/products', name: 'all_products', methods: ['GET'])]
    public function index(): Response
    {
        $products = $this->productService->findAll();
        return $this->handleView($this->view($products));
    }

    #[Route('/products/add/', name: 'add_product', methods: ['POST'])]
    public function addProduct(Request $request): Response
    {
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView($this->view($form, 400));
        }

        $product = $this->productService->create($product);
        return $this->handleView($this->view($product));
    }

    #[Route('/products/{id}', name: 'get_one_product', methods: ['GET'])]
    public function getProduct($id): Response
    {
        $product = $this->productService->find($id);
        return $this->handleView($this->view($product));
    }

    #[Route('/products/{id}/update/', name: 'update_one_product', methods: ['PATCH'])]
    public function  updateProduct($id, Request $request ): Response
    {
        $product = $this->productService->find($id);
        if(!$product){
            throw $this->createNotFoundException(
              'No product found'
            );
        }
        $form = $this->createForm(ProductType::class, $product, ['method' => 'patch']);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView($this->view($form, 400));
        }

        $product = $this->productService->update($product);
        return $this->handleView($this->view($product));
    }

    #[Route('products/{id}/delete/', name: 'delete_one_product', methods: ['DELETE'])]
    public function deleteProduct($id): Response
    {
        $product = $this->productService->find($id);
        if(!$product){
            throw $this->createNotFoundException(
                'No product found'
            );
        }
        $this->productService->delete($product);
        return $this->handleView($this->view(['status' => 'ok']));
    }
}
