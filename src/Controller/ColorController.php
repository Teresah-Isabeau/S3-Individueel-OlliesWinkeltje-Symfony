<?php

namespace App\Controller;

use App\Entity\Color;
use App\Form\ColorType;
use App\Service\ColorService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ColorController extends AbstractFOSRestController
{
    private ColorService $colorService;

    public function __construct(
        ColorService $colorService
    ) {
        $this->colorService = $colorService;
    }

    #[Route('/colors', name: 'all_colors', methods: ['GET'])]
    public function index(): Response
    {
        $colors = $this->colorService->findAll();
        return $this->handleView($this->view($colors));
    }

    #[Route('/colors/add/', name: 'add_color', methods: ['POST'])]
    public function addColor(Request $request): Response
    {
        $color = new Color();

        $form = $this->createForm(ColorType::class, $color);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView($this->view($form));
        }

        $color = $this->colorService->create($color);
        return $this->handleView($this->view($color));
    }

    #[Route('/colors/{id}', name: 'get_one_color', methods: ['GET'])]
    public function getColor($id): Response
    {
        $color = $this->colorService->find($id);
        return $this->handleView($this->view($color));
    }

    #[Route('/colors/{id}/update/', name: 'update_one_color', methods: ['PATCH'])]
    public function  updateColor($id, Request $request ): Response
    {
        $color = $this->colorService->find($id);
        if(!$color){
            throw $this->createNotFoundException(
                'No color found'
            );
        }
        $form = $this->createForm(ColorType::class, $color, ['method' => 'patch']);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->handleView($this->view($form, 400));
        }

        $color = $this->colorService->update($color);
        return $this->handleView($this->view($color));
    }

    #[Route('colors/{id}/delete/', name: 'delete_one_color', methods: ['DELETE'])]
    public function deleteColor($id): Response
    {
        $color = $this->colorService->find($id);
        if(!$color){
            throw $this->createNotFoundException(
                'No color found'
            );
        }
        $this->colorService->delete($color);
        return $this->handleView($this->view(['status' => 'ok']));
    }
}
